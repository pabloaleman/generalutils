/**
 * Clase FilesUtilTest.java mar 27, 2019
 */
package com.megasoftworks.utils.files.test;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

import org.junit.Assert;
import org.junit.Test;

import com.megasoftworks.utils.files.FilesUtil;
import com.megasoftworks.utils.files.exceptions.ConversionArchivosException;
import com.megasoftworks.utils.files.exceptions.CrearArchivoException;
import com.megasoftworks.utils.files.exceptions.LeerArchivoException;

/**
 * @author pabloaleman
 */
public class FilesUtilTest {
    private static final String mensajeError = "Error en la prueba";
    private static final String mensajeEscribir = "LINEA1\nLINEA2\nLINEA3";
    private static final String pathSalidaBA = "src/test/resources/binarioSalida.txt";
    private static final String pathSalida = "src/test/resources/salida.txt";

    @Test
    public void crearArchivoDesdeBA() {
        try {
            FilesUtil.crearArchivoDesdeBA(mensajeEscribir.getBytes(), pathSalidaBA);
            Assert.assertTrue(mensajeError, FilesUtil.existeArchivo(pathSalidaBA));
        } catch (CrearArchivoException e) {
            Assert.fail(mensajeError);
        }
    }

    @Test
    public void escribirArchivo() {
        try {
            FilesUtil.escribirArchivo(pathSalida, mensajeEscribir);
            Assert.assertTrue(mensajeError, FilesUtil.existeArchivo(pathSalida));
        } catch (CrearArchivoException e) {
            Assert.fail(mensajeError);
        }
    }

    @Test
    public void leerArchivo() {
        try {
            String retorno = FilesUtil.leerArchivo(pathSalida);
            Assert.assertEquals(mensajeError, mensajeEscribir, retorno.trim());
        } catch (LeerArchivoException e) {
            Assert.fail(mensajeError);
        }
    }

    @Test
    public void inputStreamToString() {
        try {
            FileInputStream fis = new FileInputStream(pathSalida);
            String retorno = FilesUtil.inputStreamToString(fis);
            Assert.assertEquals(mensajeError, mensajeEscribir, retorno.trim());
        } catch (ConversionArchivosException | FileNotFoundException e) {
            Assert.fail(mensajeError);
        }
    }

}
