/**
 * Clase LeerArchivoException.java mar 22, 2019
 */
package com.megasoftworks.utils.files.exceptions;

/**
 * @author pabloaleman
 */
public class LeerArchivoException extends Exception {
    private static final long serialVersionUID = -7189324710950932556L;
    public LeerArchivoException(Throwable t) {
        super(t);
    }
}
