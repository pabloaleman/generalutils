/**
 * Clase CrearArchivoException.java mar 22, 2019
 */
package com.megasoftworks.utils.files.exceptions;

/**
 * @author pabloaleman
 */
public class CrearArchivoException extends Exception {
    private static final long serialVersionUID = 29344454428071211L;

    public CrearArchivoException(Throwable t) {
        super(t);
    }
}
