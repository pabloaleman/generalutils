/**
 * Clase ParametroNoEncontradoException.java abr 01, 2019
 */
package com.megasoftworks.utils.propiedades.exceptions;

/**
 * @author pabloaleman
 */
public class ParametroNoEncontradoException extends Exception {
    private static final long serialVersionUID = 7414457123704444286L;
}
