/**
 * Clase RequestInterceptor.java mar 19, 2019
 */
package com.megasoftworks.utils.soap.client.cxf.interceptors;

import java.io.OutputStream;

import org.apache.cxf.interceptor.LoggingOutInterceptor;
import org.apache.cxf.io.CacheAndWriteOutputStream;
import org.apache.cxf.io.CachedOutputStream;
import org.apache.cxf.io.CachedOutputStreamCallback;
import org.apache.cxf.message.Message;
import org.apache.cxf.phase.Phase;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author pabloaleman
 */
public class RequestInterceptor extends LoggingOutInterceptor {
    static final Logger LOGGER = LoggerFactory.getLogger(RequestInterceptor.class);


    public RequestInterceptor() {
        super(Phase.PRE_STREAM);
    }

    @Override
    public void handleMessage(Message message) {
        try {
            OutputStream out = message.getContent(OutputStream.class);
            final CacheAndWriteOutputStream newOut = new CacheAndWriteOutputStream(out);
            message.setContent(OutputStream.class, newOut);
            newOut.registerCallback(new LoggingCallback());
        } catch (Exception e) {
            LOGGER.warn("Error general en el interceptor request", e);
        }
    }

    private class LoggingCallback implements CachedOutputStreamCallback {
        public void onFlush(CachedOutputStream cos) {
            LOGGER.debug("entrando a onFlush");
        }

        public void onClose(CachedOutputStream cos) {
            try {
                StringBuilder builder = new StringBuilder();
                cos.writeCacheTo(builder, limit);
                String requestXml = builder.toString();
                LOGGER.info("Request XML: {}", requestXml);
            } catch (Exception e) {
                LOGGER.warn("Error en el interceptor response {}", e.getMessage());
            }
        }
    }
}
