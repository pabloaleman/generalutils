/**
 * Clase ConverterException.java mar 19, 2019
 */
package com.megasoftworks.utils.soap.client.exceptions;

/**
 * @author pabloaleman
 */
public class ConverterException extends Exception {

    private static final long serialVersionUID = 6382097291796216263L;

    public ConverterException(Throwable e) {
        super(e);
    }
}
