/**
 * Clase ResponseInterceptor.java mar 19, 2019
 */
package com.megasoftworks.utils.soap.client.cxf.interceptors;

import java.io.InputStream;

import org.apache.cxf.helpers.IOUtils;
import org.apache.cxf.interceptor.LoggingInInterceptor;
import org.apache.cxf.io.CachedOutputStream;
import org.apache.cxf.io.CachedOutputStreamCallback;
import org.apache.cxf.message.Message;
import org.apache.cxf.phase.Phase;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author pabloaleman
 */
public class ResponseInterceptor extends LoggingInInterceptor {

    static final Logger LOGGER = LoggerFactory.getLogger(ResponseInterceptor.class);

    public ResponseInterceptor() {
        super(Phase.RECEIVE);
    }

    @Override
    public void handleMessage(Message message) {
        try {
            InputStream is = message.getContent(InputStream.class);
            CachedOutputStream bos = new CachedOutputStream();
            IOUtils.copy(is, bos);
            bos.flush();
            message.setContent(InputStream.class, bos.getInputStream());
            is.close();
            bos.registerCallback(new LoggingCallback());
            bos.close();
        } catch (Exception e) {
            LOGGER.warn("Error en el interceptor response {}", e.getMessage());
        }
    }

    private class LoggingCallback implements CachedOutputStreamCallback {
        public void onFlush(CachedOutputStream cos) {
            LOGGER.debug("entrando a onFlush");
        }

        public void onClose(CachedOutputStream cos) {
            try {
                StringBuilder builder = new StringBuilder();
                cos.writeCacheTo(builder, limit);
                String responseXml = builder.toString();
                LOGGER.info("Response XML: {}", responseXml);
            } catch (Exception e) {
                LOGGER.warn("Error en el interceptor response {}", e.getMessage());
            }
        }
    }
}