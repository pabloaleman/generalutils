/**
 * Clase ErrorObteniendoInformacionUsuarioException.java mar 22, 2019
 */
package com.megasoftworks.utils.soap.server.exceptions;

/**
 * @author pabloaleman
 */
public class ErrorObteniendoInformacionUsuarioException extends Exception {
    private static final long serialVersionUID = -3446209897173710896L;

    public ErrorObteniendoInformacionUsuarioException() {
        super("There was an error while decoding the Authentication!");
    }
}
