/**
 * Clase WebServiceConsumerPort.java mar 19, 2019
 */
package com.megasoftworks.utils.soap.client.constructores;

import org.apache.cxf.endpoint.Client;
import org.apache.cxf.frontend.ClientProxy;
import org.apache.cxf.jaxws.JaxWsProxyFactoryBean;
import org.apache.cxf.transport.http.HTTPConduit;
import org.apache.cxf.transports.http.configuration.HTTPClientPolicy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.megasoftworks.utils.soap.client.cxf.interceptors.RequestInterceptor;
import com.megasoftworks.utils.soap.client.cxf.interceptors.ResponseInterceptor;
import com.megasoftworks.utils.soap.client.exceptions.PuertoNullException;

/**
 * @author pabloaleman
 */
public abstract class WebServiceConsumerPort<P> {


    private static Logger log = LoggerFactory.getLogger(WebServiceConsumerPort.class);
    protected P puerto;
    private String nombreWs;

    public void creaPuerto(String nombreWsP, String url, String timeoutS,
                           boolean requestInterceptor, boolean responseInterceptor,
                           String user, String password, Class<P> tipoClase) {
        nombreWs = nombreWsP;
        try {
            Long timeout = Long.parseLong(timeoutS);
            JaxWsProxyFactoryBean factory = new JaxWsProxyFactoryBean();
            factory.setServiceClass(tipoClase);
            factory.setAddress(url);
            if (requestInterceptor) {
                factory.getOutInterceptors().add(new RequestInterceptor());
            }
            if (responseInterceptor) {
                factory.getInInterceptors().add(new ResponseInterceptor());
            }
            if (user != null) {
                factory.setUsername(user);
                factory.setPassword(password);
            }
            puerto = (P) factory.create();

            Client client = ClientProxy.getClient(puerto);

            if (client != null) {

                log.info("Timeout Servicios Web - {}: {}", nombreWs, timeout);

                HTTPConduit conduit = (HTTPConduit) client.getConduit();
                HTTPClientPolicy policy = new HTTPClientPolicy();
                policy.setAllowChunking(false);
                policy.setConnectionTimeout(timeout * 1000);
                policy.setReceiveTimeout(timeout * 1000);
                conduit.setClient(policy);
            }
        } catch (Exception e) {
            log.error("Problemas al crear el puerto del web service de " + nombreWs, e);
            puerto = null;
        }
    }

    public void compruebaPuertoNoNull() throws PuertoNullException {
        if (puerto == null) {
            throw new PuertoNullException("Puerto del servicio web " + nombreWs + " es null");
        }
    }
}
