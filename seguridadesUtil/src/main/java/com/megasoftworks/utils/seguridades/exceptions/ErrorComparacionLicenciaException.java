/**
 * Clase ErrorComparacionLicenciaException.java abr 03, 2019
 */
package com.megasoftworks.utils.seguridades.exceptions;

/**
 * @author pabloaleman
 */
public class ErrorComparacionLicenciaException extends Exception {
    private static final long serialVersionUID = 6869265150611275588L;
}
