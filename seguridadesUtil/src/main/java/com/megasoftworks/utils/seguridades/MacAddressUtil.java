/**
 * Clase NetUtil.java abr 03, 2019
 */
package com.megasoftworks.utils.seguridades;

import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.megasoftworks.utils.seguridades.beans.DireccionMac;

/**
 * @author pabloaleman
 */
public final class MacAddressUtil {
    private static final Logger LOGGER = LoggerFactory.getLogger(MacAddressUtil.class);

    private MacAddressUtil() {

    }

    public static List<DireccionMac> obtenerDireccionesMac() {
        List<DireccionMac> direccionesMac = new ArrayList<>();
        try {
            Enumeration<NetworkInterface> networkInterface = NetworkInterface.getNetworkInterfaces();
            while (networkInterface.hasMoreElements()) {
                obtenerDireccionDesdeNetworkInterface(networkInterface.nextElement(), direccionesMac);
            }
        } catch (SocketException e) {
            LOGGER.error("Error obteniendo la direccion MAC", e);
        }
        return direccionesMac;
    }

    private static void obtenerDireccionDesdeNetworkInterface(NetworkInterface network, List<DireccionMac> lista) {
        try {
            byte[] macAddressBytes = network.getHardwareAddress();
            if (macAddressBytes != null) {
                DireccionMac direccionMac = new DireccionMac();
                direccionMac.setNombreInterface(network.getName());
                StringBuilder macAddressStr = new StringBuilder();
                for (int i = 0; i < macAddressBytes.length; i++) {
                    macAddressStr.append(String.format("%02X", macAddressBytes[i]));
                }
                direccionMac.setDireccionMacComputador(macAddressStr.toString());
                lista.add(direccionMac);
            }
        } catch (SocketException e) {
            LOGGER.error("Error obteniendo la direccion MAC", e);
        }
    }
}
