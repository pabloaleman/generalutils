/**
 * Clase Md5UtilTest.java mar 29, 2019
 */
package com.megasoftworks.utils.md5.test;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

import org.junit.Assert;
import org.junit.Test;

import com.megasoftworks.utils.md5.Md5Util;
import com.megasoftworks.utils.md5.exceptions.CreacionMd5Exception;

/**
 * @author pabloaleman
 */
public class Md5UtilTest {

    public static final String cadena = "12345";
    public static final String md5Cadena = "827CCB0EEA8A706C4C34A16891F84E7B";
    public static final String md5Archivo = "42c6743108656e82399ab08577389774";
    String pathArchivo = "src/test/resources/rdc.png";

    @Test
    public void fileMd5Sum() {
        try {
            String resultado = Md5Util.fileMd5Sum(new FileInputStream(pathArchivo));
            Assert.assertEquals(md5Archivo, resultado);
        } catch (FileNotFoundException | CreacionMd5Exception e) {
            Assert.fail();
        }
    }

    @Test
    public void fileMd5SumPath() {
        try {
            String resultado = Md5Util.fileMd5Sum(pathArchivo);
            Assert.assertEquals(md5Archivo, resultado);
        } catch (CreacionMd5Exception e) {
            Assert.fail();
        }
    }

    @Test
    public void toMD5() {
        Assert.assertEquals(md5Cadena, Md5Util.toMD5(cadena).toUpperCase());
    }

}


/*
ftpUtil
        - agregar exceptions
        - crear pruebas de unidad
        Jasper
        -agregar pruebas de unidad.
        mail
        - agregar pruebas de unidad.
        sriutil
        - agregar exceptions
        - crear pruebas de unidad

        probar networkutil

        soaputil pruebas de unidad

        primefaces no tiene pruebas de unidad

        jsfValidadoresUtil no tiene pruebas de unidad

        captchautil pendiente

        firma documentos	posiblemente arreglar y javadoc

        jsfutil
        revisar funciones
        javadoc
        */