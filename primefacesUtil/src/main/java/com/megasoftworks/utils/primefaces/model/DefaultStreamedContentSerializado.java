/**
 * Clase DefaultStreamedContentSerializado.java mar 19, 2019
 */
package com.megasoftworks.utils.primefaces.model;

import java.io.InputStream;
import java.io.Serializable;

import org.primefaces.model.DefaultStreamedContent;

/**
 * @author pabloaleman
 */
public class DefaultStreamedContentSerializado extends DefaultStreamedContent implements Serializable {
    private static final long serialVersionUID = 4905006024892824091L;

    public DefaultStreamedContentSerializado(InputStream stream) {
        super(stream);
    }

    public DefaultStreamedContentSerializado(InputStream stream, String contentType) {
        super(stream, contentType);
    }

    public DefaultStreamedContentSerializado(InputStream stream, String contentType, String name) {
        super(stream, contentType, name);
    }

    public DefaultStreamedContentSerializado(InputStream stream, String contentType, String name, String contentEncoding) {
        super(stream, contentType, name, contentEncoding);
    }

    public DefaultStreamedContentSerializado(InputStream stream, String contentType, String name, Integer contentLength) {
        super(stream, contentType, name, contentLength);
    }

    public DefaultStreamedContentSerializado(InputStream stream, String contentType, String name, String contentEncoding, Integer contentLength) {
        super(stream, contentType, name, contentEncoding, contentLength);
    }

}
