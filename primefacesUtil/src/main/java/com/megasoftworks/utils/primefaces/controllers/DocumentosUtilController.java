/**
 * Clase DocumentosUtilController.java mar 19, 2019
 */
package com.megasoftworks.utils.primefaces.controllers;

import java.io.Serializable;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import com.megasoftworks.utils.primefaces.model.DefaultStreamedContentSerializado;

/**
 * @author pabloaleman
 */
@SessionScoped
@ManagedBean
public class DocumentosUtilController implements Serializable {
    private static final long serialVersionUID = 5156185221110538565L;

    private DefaultStreamedContentSerializado fileSC;
    private String fileString;
    private Long id;

    public DefaultStreamedContentSerializado getFileSC() {
        return fileSC;
    }

    public void setFileSC(DefaultStreamedContentSerializado fileSC) {
        this.fileSC = fileSC;
    }

    public String getFileString() {
        return fileString;
    }

    public void setFileString(String fileString) {
        this.fileString = fileString;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
