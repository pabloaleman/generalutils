/**
 * Clase Redireccion.java mar 19, 2019
 */
package com.megasoftworks.utils.jsf;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

/**
 * @author pabloaleman
 */
public final class Redireccion {

    private static final Logger logger = LoggerFactory.getLogger(Redireccion.class);

    private Redireccion() {

    }

    public static void redirecciona(String path) {
        ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
        try {
            context.getFlash().setKeepMessages(true);
            context.redirect(path);
        } catch (IOException e) {
            logger.error(String.format("Error al redireccionar a %s", path), e);
        }
    }


    public static void redirecciona(String path, boolean flash) {
        ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
        try {
            context.getFlash().setKeepMessages(flash);
            context.redirect(path);
        } catch (IOException e) {
            logger.error(String.format("Error al redireccionar a %s", path), e);
        }
    }

}
