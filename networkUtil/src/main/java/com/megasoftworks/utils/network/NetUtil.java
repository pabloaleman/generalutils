/**
 * Clase NetUtil.java mar 19, 2019
 */
package com.megasoftworks.utils.network;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.InetAddress;

/**
 * @author pabloaleman
 */
public final class NetUtil {

    static final Logger LOGGER = LoggerFactory.getLogger(NetUtil.class);

    private NetUtil() {

    }

    /**
     * Funcion que evalua si hay ping a una ip dada.
     *
     * @param address la ip a evaluar si hay ping.
     * @return si hay ping o no.
     */
    public static boolean hacePing(String address) {
        try {
            InetAddress inet = InetAddress.getByName(address);
            return inet.isReachable(5000);
        } catch (Exception e) {
            String mensaje = String.format("Error doing ping to: %s", address);
            LOGGER.error(mensaje, e);
            return false;
        }
    }

}
