/**
 * Clase GetHttpRequestResponseCode.java mar 19, 2019
 */
package com.megasoftworks.utils.network;

import java.net.HttpURLConnection;
import java.net.URL;

/**
 * @author pabloaleman
 */
public final class GetHttpRequestResponseCode {

    private static final String USER_AGENT = "Mozilla/5.0";

    private GetHttpRequestResponseCode() {
    }

    /**
     * Funcion que evalua el codigo de respuesta de una peticion a un url.
     *
     * @param url la url a evaluar.
     * @return el codigo de respuesta.
     */
    public static int evaluateURL(String url) {
        try {
            URL obj = new URL(url);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();
            con.setRequestMethod("GET");
            con.setRequestProperty("User-Agent", USER_AGENT);
            return con.getResponseCode();
        } catch (Exception e) {
            return -1;
        }
    }
}
