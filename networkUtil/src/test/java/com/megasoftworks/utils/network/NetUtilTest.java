/**
 * Clase NetUtilTest.java mar 27, 2019
 */
package com.megasoftworks.utils.network;

import org.junit.Assert;
import org.junit.Test;

/**
 * @author pabloaleman
 */
public class NetUtilTest {
    private static final String mensajeError = "Error en la prueba";

    @Test
    public void hacePing() {
        boolean retorno = NetUtil.hacePing("localhost");
        Assert.assertTrue(mensajeError, retorno);

        retorno = NetUtil.hacePing("localhosts");
        Assert.assertFalse(mensajeError, retorno);
    }
}
