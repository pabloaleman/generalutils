package com.megasoftworks.utils.sri;

public final class SriUtil {

    private SriUtil() {
    }

    /**
     * Función para obtener el dígito verificador de la clave de acceso en modulo 11
     *
     * @param cadena la cadena para generar el codigo
     * @return el dígito verificador
     */
    public static int getVerifierModule11(String cadena) {
        int baseMultiplicador = 7;
        int[] aux = new int[cadena.length()];
        int multiplicador = 2;
        int total = 0;
        int verificador;
        for (int i = aux.length - 1; i >= 0; i--) {
            aux[i] = Integer.parseInt((new StringBuilder()).append(cadena.charAt(i)).toString());
            aux[i] = aux[i] * multiplicador;
            if (++multiplicador > baseMultiplicador) {
                multiplicador = 2;
            }
            total += aux[i];
        }
        // --Ya tenemos el total
        if (total == 0 || total == 1) {
            verificador = 0;
        } else {
            verificador = (11 - (total % 11)) != 11 ? 11 - total % 11 : 0;
        }
        if (verificador == 10) {
            verificador = 1;
        }
        return verificador;
    }

}
