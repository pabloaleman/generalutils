/**
 * Clase ConversionNumerosException.java mar 22, 2019
 */
package com.megasoftworks.utils.numeros;

import com.megasoftworks.utils.numeros.exceptions.ConversionNumerosException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;

/**
 * @author pabloaleman
 */
public final class NumerosUtil {

    private static final Logger LOGGER = LoggerFactory.getLogger(NumerosUtil.class);

    private NumerosUtil() {
    }

    /**
     * Retorna una cantidad numerica en la cantidad de decimales dado.
     *
     * @param numero     el numero a convertir.
     * @param formato    el formato del numero.
     * @param nDecimales el numero de decmales que se requiere que devuelva.
     * @return el String que representa el numero.
     * @throws ConversionNumerosException en caso de errores de conversion.
     */
    public static String getNumeroFormateado(Object numero, String formato, int nDecimales) throws ConversionNumerosException {
        try {
            DecimalFormat formatoNumero;
            DecimalFormatSymbols idfSimbolos = new DecimalFormatSymbols();
            idfSimbolos.setDecimalSeparator('.');
            formatoNumero = new DecimalFormat(formato, idfSimbolos);
            formatoNumero.setMaximumFractionDigits(nDecimales);
            formatoNumero.setMinimumFractionDigits(nDecimales);
            double ldobValor = Double.parseDouble(numero.toString());
            return formatoNumero.format(ldobValor);
        } catch (Exception e) {
            LOGGER.error(String.format("Error al formatear el numero %s",numero), e);
            throw new ConversionNumerosException("Error", e);
        }
    }

    /**
     * Funcion que evalua si un string puede representar un numero entero.
     *
     * @param numero la cadena a evaluar.
     * @return si es entero o no.
     */
    public static boolean isInteger(String numero) {
        if (numero == null) {
            return false;
        }
        try {
            Integer.parseInt(numero);
            return true;
        } catch (NumberFormatException nfe) {
            return false;
        }
    }

    /**
     * Funcion que evalua si un string puede representar un numero double.
     *
     * @param numero la cadena a evaluar.
     * @return si es double o no.
     */
    public static boolean isDouble(String numero) {
        if (numero == null) {
            return false;
        }
        try {
            Double.parseDouble(numero);
            return true;
        } catch (NumberFormatException nfe) {
            return false;
        }
    }
}
