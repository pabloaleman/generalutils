/**
 * Clase VelocityUtil.java mar 19, 2019
 */
package com.megasoftworks.utils.velocity;

import java.io.StringReader;
import java.io.StringWriter;
import java.util.Map;

import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.runtime.RuntimeServices;
import org.apache.velocity.runtime.RuntimeSingleton;
import org.apache.velocity.runtime.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.megasoftworks.utils.velocity.exceptions.VelocityUtilException;

/**
 * @author pabloaleman
 */
public final class VelocityUtil {

    private static final Logger LOGGER = LoggerFactory.getLogger(VelocityUtil.class);

    private VelocityUtil() {

    }


    /**
     * Funcion que reemplaza las cadenas en el template de velocity.
     * @param textoTemplate el template.
     * @param campos los campos a reemplazar.
     * @return el string resultado de la transformacion.
     * @throws VelocityUtilException en caso de errores.
     */
    public static String replace(String textoTemplate, Map<String, String> campos) throws VelocityUtilException {
        try {
            RuntimeServices rs = RuntimeSingleton.getRuntimeServices();
            StringReader sr = new StringReader(textoTemplate);

            VelocityEngine engine = new VelocityEngine();
            engine.init();

            Template template = new Template();
            template.setRuntimeServices(rs);
            template.setData(rs.parse(sr, template));
            template.initDocument();

            VelocityContext velocityContext = new VelocityContext();
            for (Map.Entry<String, String> entry : campos.entrySet()) {
                velocityContext.put(entry.getKey(), entry.getValue());
            }

            StringWriter sw = new StringWriter();
            template.merge(velocityContext, sw);

            return sw.toString();
        } catch (ParseException e) {
            LOGGER.error("Problema tipo parse", e);
            throw new VelocityUtilException(e);
        }

    }

}
