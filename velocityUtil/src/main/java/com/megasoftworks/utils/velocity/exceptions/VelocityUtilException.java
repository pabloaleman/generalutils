/**
 * Clase VelocityUtilException.java mar 27, 2019
 */
package com.megasoftworks.utils.velocity.exceptions;

/**
 * @author pabloaleman
 */
public class VelocityUtilException extends Exception {
    private static final long serialVersionUID = 1504262235117503184L;

    public VelocityUtilException(Throwable t) {
        super(t);
    }
}
