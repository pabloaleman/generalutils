/**
 * Clase VelocityUtilTest.java mar 27, 2019
 */
package com.megasoftworks.utils.velocity.test;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.junit.Assert;
import org.junit.Test;

import com.megasoftworks.utils.velocity.VelocityUtil;
import com.megasoftworks.utils.velocity.exceptions.VelocityUtilException;

/**
 * @author pabloaleman
 */
public class VelocityUtilTest {

    private static final String mensajeError = "Error en la prueba";

    @Test
    public void replace() {
        try {
            String textoTemplate = "Este es un ejemplo de saludo $saludo a $nombres";
            String textoEsperado = "Este es un ejemplo de saludo Hola a Pablin";
            Map<String, String> campos = new ConcurrentHashMap<>();
            campos.put("saludo", "Hola");
            campos.put("nombres", "Pablin");
            String retornado = VelocityUtil.replace(textoTemplate, campos);
            Assert.assertEquals(mensajeError, textoEsperado, retornado);
        } catch (VelocityUtilException e) {
            Assert.fail(mensajeError);
        }

    }
}
