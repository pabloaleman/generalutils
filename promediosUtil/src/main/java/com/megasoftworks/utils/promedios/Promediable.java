/**
 * Clase Promediable.java abr 01, 2019
 */
package com.megasoftworks.utils.promedios;

/**
 * @author pabloaleman
 */
public interface Promediable {
	Double getToAvgValue();
}
