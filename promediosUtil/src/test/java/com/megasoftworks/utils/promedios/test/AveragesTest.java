/**
 * Clase AveragesTest.java abr 01, 2019
 */
package com.megasoftworks.utils.promedios.test;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.megasoftworks.utils.promedios.Averages;
import com.megasoftworks.utils.promedios.Promediable;
import com.megasoftworks.utils.promedios.exceptions.AverageListaNullVaciaException;

/**
 * @author pabloaleman
 */
public class AveragesTest {
    private static final double DELTA = 1e-15;
    private List<Promediable> promediablesList;

    @Before
    public void init() {
        promediablesList = new ArrayList<>();
        promediablesList.add(new PromediableImpl(2.0));
        promediablesList.add(new PromediableImpl(3.0));
        promediablesList.add(new PromediableImpl(4.0));
        promediablesList.add(new PromediableImpl(5.0));
    }

    @Test
    public void getLogarithmicAverage() {
        try {
            double retornado = Averages.getLogarithmicAverage(promediablesList);
            Assert.assertEquals(0.0, retornado, DELTA);
        } catch (AverageListaNullVaciaException e) {
            Assert.fail();
        }
    }

    @Test
    public void getAverage() {
        try {
            double retornado = Averages.getAverage(promediablesList);
            Assert.assertEquals(3.5, retornado, DELTA);
        } catch (AverageListaNullVaciaException e) {
            Assert.fail();
        }
    }

    @Test
    public void getLogarithmicAverageListaVacia() {
        try {
            Averages.getLogarithmicAverage(new ArrayList<PromediableImpl>());
            Assert.fail();
        } catch (AverageListaNullVaciaException e) {
            Assert.assertNotNull(e);
        }
    }

    @Test
    public void getAverageListaVacia() {
        try {
            Averages.getAverage(new ArrayList<PromediableImpl>());
            Assert.fail();
        } catch (AverageListaNullVaciaException e) {
            Assert.assertNotNull(e);
        }
    }

    @Test
    public void getLogarithmicAverageListaNull() {
        try {
            Averages.getLogarithmicAverage(null);
            Assert.fail();
        } catch (AverageListaNullVaciaException e) {
            Assert.assertNotNull(e);
        }
    }

    @Test
    public void getAverageListaNull() {
        try {
            Averages.getAverage(null);
            Assert.fail();
        } catch (AverageListaNullVaciaException e) {
            Assert.assertNotNull(e);
        }
    }
}
