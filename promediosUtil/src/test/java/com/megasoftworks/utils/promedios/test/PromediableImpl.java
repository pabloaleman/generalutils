/**
 * Clase PromediableImpl.java abr 01, 2019
 */
package com.megasoftworks.utils.promedios.test;

import com.megasoftworks.utils.promedios.Promediable;

/**
 * @author pabloaleman
 */
public class PromediableImpl implements Promediable {
    private double valor;

    public PromediableImpl(double valor) {
        this.valor = valor;
    }

    @Override
    public Double getToAvgValue() {
        return valor;
    }
}
