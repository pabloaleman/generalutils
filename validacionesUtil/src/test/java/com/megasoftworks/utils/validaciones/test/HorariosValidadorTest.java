/**
 * Clase HorariosValidadorTest.java mar 27, 2019
 */
package com.megasoftworks.utils.validaciones.test;

import org.junit.Assert;
import org.junit.Test;

import com.megasoftworks.utils.validaciones.ConstantesValidaciones;
import com.megasoftworks.utils.validaciones.HorariosValidador;

/**
 * @author pabloaleman
 */
public class HorariosValidadorTest {
    private static final String mensajeError = "Error en la prueba";
    private static final String horario = "08:00-12:00,13:00-17:00";
    private static final String horarioSolo = "08:00-12:00";
    private static final String separadorHoras = "-";
    private static final String separadorHorarios = ",";
    @Test
    public void validaHorarioAtencion() {
        Assert.assertNull(mensajeError, HorariosValidador.validaHorarioAtencion(horario, separadorHorarios, separadorHoras));

        String horarioError = "08:0017:00";
        Assert.assertEquals(mensajeError, ConstantesValidaciones.ERROR_FORMATO_HORARIO,
                HorariosValidador.validaHorarioAtencion(horarioError, separadorHorarios, separadorHoras));

        horarioError = "08:00-1700";
        Assert.assertEquals(mensajeError, ConstantesValidaciones.ERROR_FORMATO_HORARIO,
                HorariosValidador.validaHorarioAtencion(horarioError, separadorHorarios, separadorHoras));

        horarioError = "08:00-17:100";
        Assert.assertEquals(mensajeError, ConstantesValidaciones.ERROR_FORMATO_HORARIO,
                HorariosValidador.validaHorarioAtencion(horarioError, separadorHorarios, separadorHoras));

        horarioError = "17:00-08:00";
        Assert.assertEquals(mensajeError, ConstantesValidaciones.ERROR_DIFERENCIA_HORARIO,
                HorariosValidador.validaHorarioAtencion(horarioError, separadorHorarios, separadorHoras));

        horarioError = "08:00-12:00,11:00-17:00";
        Assert.assertEquals(mensajeError, ConstantesValidaciones.ERROR_CRUCE_HORARIOS,
                HorariosValidador.validaHorarioAtencion(horarioError, separadorHorarios, separadorHoras));

        horarioError = "08:00-a8:00";
        Assert.assertEquals(mensajeError, ConstantesValidaciones.ERROR_FORMATO_HORARIO,
                HorariosValidador.validaHorarioAtencion(horarioError, separadorHorarios, separadorHoras));
    }

    @Test
    public void validaIntervalosTiempo() {
        String formatoFechas = "HH:mm";
        Assert.assertNull(mensajeError, HorariosValidador.validaIntervalosTiempo(horarioSolo, formatoFechas, separadorHoras));

        String horarioError = "08:0017:00";
        Assert.assertEquals(mensajeError, ConstantesValidaciones.ERROR_FORMATO_INTERVALO,
                HorariosValidador.validaIntervalosTiempo(horarioError, formatoFechas, separadorHoras));

        horarioError = "aa:00-17:00";
        Assert.assertEquals(mensajeError, ConstantesValidaciones.ERROR_FECHA_INICIO_INTERVALO,
                HorariosValidador.validaIntervalosTiempo(horarioError, formatoFechas, separadorHoras));

        horarioError = "08:00-aa:00";
        Assert.assertEquals(mensajeError, ConstantesValidaciones.ERROR_FECHA_FIN_INTERVALO,
                HorariosValidador.validaIntervalosTiempo(horarioError, formatoFechas, separadorHoras));

        horarioError = "17:00-08:00";
        Assert.assertEquals(mensajeError, ConstantesValidaciones.ERROR_FECHA_INICIO_MAYOR_A_FECHA_FIN_INTERVALO,
                HorariosValidador.validaIntervalosTiempo(horarioError, formatoFechas, separadorHoras));
    }

}
