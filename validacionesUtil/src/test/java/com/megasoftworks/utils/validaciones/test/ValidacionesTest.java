/**
 * Clase ValidacionesTest.java mar 29, 2019
 */
package com.megasoftworks.utils.validaciones.test;

import org.junit.Assert;
import org.junit.Test;

import com.megasoftworks.utils.validaciones.ConstantesValidaciones;
import com.megasoftworks.utils.validaciones.Validaciones;

/**
 * @author pabloaleman
 */
public class ValidacionesTest {
    private static final String mensajeError = "Error en la prueba";
    private static final String cedula = "1714365861";

    @Test
    public void validaCedula() {
        Assert.assertNull(mensajeError, Validaciones.validaCedula(cedula));
        Assert.assertEquals(mensajeError, ConstantesValidaciones.ERROR_CEDULA_NULL_EMPTY,
                Validaciones.validaCedula(null));

        Assert.assertEquals(mensajeError, ConstantesValidaciones.ERROR_CEDULA_TAMANIO,
                Validaciones.validaCedula("12"));

        Assert.assertEquals(mensajeError, ConstantesValidaciones.ERROR_CEDULA_NO_NUMEROS,
                Validaciones.validaCedula("123456789f"));

        Assert.assertEquals(mensajeError, ConstantesValidaciones.ERROR_CEDULA_NO_VALIDA,
                Validaciones.validaCedula("1714365862"));
        Assert.assertEquals(mensajeError, ConstantesValidaciones.ERROR_CEDULA_NULL_EMPTY,
                Validaciones.validaCedula(null));
    }

    @Test
    public void validaCedulaNoGuion() {
        Assert.assertNull(mensajeError, Validaciones.validaCedula(cedula));
        Assert.assertEquals(mensajeError, ConstantesValidaciones.ERROR_CEDULA_CON_GUION,
                Validaciones.validaCedulaNoGuion("171436586-1"));
        Assert.assertEquals(mensajeError, ConstantesValidaciones.ERROR_CEDULA_NULL_EMPTY,
                Validaciones.validaCedulaNoGuion(null));
    }

    @Test
    public void validaRuc() {
        Assert.assertNull(mensajeError, Validaciones.validaRuc("1714365861001"));
        Assert.assertEquals(mensajeError, ConstantesValidaciones.ERROR_RUC_TAMANIO,
                Validaciones.validaRuc(cedula));
        Assert.assertEquals(mensajeError, ConstantesValidaciones.ERROR_RUC_NO_NUMEROS,
                Validaciones.validaRuc("1714365861ooq"));
        Assert.assertEquals(mensajeError, ConstantesValidaciones.ERROR_RUC_NO_VALIDO,
                Validaciones.validaRuc("1714365862001"));
        Assert.assertEquals(mensajeError, ConstantesValidaciones.ERROR_RUC_NO_VALIDO,
                Validaciones.validaRuc("1714365861000"));
        Assert.assertEquals(mensajeError, ConstantesValidaciones.ERROR_RUC_VACIO_NULO,
                Validaciones.validaRuc(null));
    }

    @Test
    public void validaCedulaRuc() {
        Assert.assertNull(mensajeError, Validaciones.validaCedulaRuc("1714365861001"));
        Assert.assertNull(mensajeError, Validaciones.validaCedulaRuc(cedula));
        Assert.assertEquals(mensajeError, ConstantesValidaciones.ERROR_CEDULA_RUC_TAMANIO,
                Validaciones.validaCedulaRuc("171436586"));
        Assert.assertEquals(mensajeError, ConstantesValidaciones.ERROR_CEDULA_RUC_NO_VALIDO,
                Validaciones.validaCedulaRuc("1714365862"));
        Assert.assertEquals(mensajeError, ConstantesValidaciones.ERROR_CEDULA_RUC_NO_VALIDO,
                Validaciones.validaCedulaRuc("1714365861000"));
        Assert.assertEquals(mensajeError, ConstantesValidaciones.ERROR_CEDULA_RUC_VACIO_NULO,
                Validaciones.validaCedulaRuc(null));
    }

    @Test
    public void validaSoloNumeros() {
        Assert.assertNull(mensajeError, Validaciones.validaSoloNumeros("123456", 6));
        Assert.assertNull(mensajeError, Validaciones.validaSoloNumeros("123456789",null));
        Assert.assertEquals(mensajeError, ConstantesValidaciones.ERROR_SOLO_NUMEROS,
                Validaciones.validaSoloNumeros("12345d", 6));
        Assert.assertEquals(mensajeError, ConstantesValidaciones.ERROR_SOLO_NUMEROS_VACIO_NULO,
                Validaciones.validaSoloNumeros(null, 6));
    }

    @Test
    public void validaEmail() {
        Assert.assertNull(mensajeError, Validaciones.validaEmail("pablo.aleman@megasoftworks.com"));
        Assert.assertEquals(mensajeError, ConstantesValidaciones.ERROR_EMAIL,
                Validaciones.validaEmail("pablo.aleman"));
        Assert.assertEquals(mensajeError, ConstantesValidaciones.ERROR_EMAIL,
                Validaciones.validaEmail("pablo.aleman@"));
        Assert.assertEquals(mensajeError, ConstantesValidaciones.ERROR_EMAIL,
                Validaciones.validaEmail("pablo.aleman@algo"));
        Assert.assertEquals(mensajeError, ConstantesValidaciones.ERROR_EMAIL_VACIO_NULO,
                Validaciones.validaEmail(null));
    }

    @Test
    public void validaCelular() {
        Assert.assertNull(mensajeError, Validaciones.validaCelular("0981980233"));
        Assert.assertEquals(mensajeError, ConstantesValidaciones.ERROR_TAMANIO_CELULAR,
                Validaciones.validaCelular("098198023"));
        Assert.assertEquals(mensajeError, ConstantesValidaciones.ERROR_CELULAR_NO_NUMEROS,
                Validaciones.validaCelular("098198023a"));
        Assert.assertEquals(mensajeError, ConstantesValidaciones.ERROR_CELULAR_NULL_EMPTY,
                Validaciones.validaCelular(null));
    }
}
