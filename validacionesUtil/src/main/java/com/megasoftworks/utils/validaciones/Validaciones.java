/**
 * Clase Validaciones.java mar 22, 2019
 */
package com.megasoftworks.utils.validaciones;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author pabloaleman
 */
public final class Validaciones {
    private Validaciones() {

    }
    /**
     * Funcion que evalua que una cédula sea válida.
     *
     * @param texto el texto a evaluar
     * @return si la cédula es correcta retornará null caso contrario retornará
     * el codigo de error (null = ok).
     */
    public static String validaCedula(String texto) {
        if (texto == null || texto.isEmpty()) {
            return ConstantesValidaciones.ERROR_CEDULA_NULL_EMPTY;
        }
        //Valido el número de caracteres = 10
        if (texto.length() != ConstantesValidaciones.TAMANIO_CEDULA) {
            return ConstantesValidaciones.ERROR_CEDULA_TAMANIO;
        }

        //Valido que sea solo números
        if (validaSoloNumeros(texto, ConstantesValidaciones.TAMANIO_CEDULA) != null) {
            return ConstantesValidaciones.ERROR_CEDULA_NO_NUMEROS;
        }

        //valida la cedula sea correcta
        try {
            int[] coefValCedula = {2, 1, 2, 1, 2, 1, 2, 1, 2};
            int verificador = Integer.parseInt(texto.substring(9, 10));
            int suma = 0;
            int digito = 0;
            for (int i = 0; i < (texto.length() - 1); i++) {
                digito = Integer.parseInt(texto.substring(i, i + 1)) * coefValCedula[i];
                suma += ((digito % 10) + (digito / 10));
            }

            if ((suma % 10 == 0) && (suma % 10 == verificador)) {
                return null;
            } else if ((10 - (suma % 10)) == verificador) {
                return null;
            } else {
                return ConstantesValidaciones.ERROR_CEDULA_NO_VALIDA;
            }
        } catch (NumberFormatException nfe) {
            return ConstantesValidaciones.ERROR_CEDULA_NO_VALIDA;
        } catch (Exception err) {
            return ConstantesValidaciones.ERROR_CEDULA_NO_VALIDA;
        }
    }

    /**
     * Funcion que valida la cédula y revisa que no tenga guion
     *
     * @param texto el texto a ser evaluado
     * @return el resultado de validacion
     */
    public static String validaCedulaNoGuion(String texto) {
        if (texto == null || texto.isEmpty()) {
            return ConstantesValidaciones.ERROR_CEDULA_NULL_EMPTY;
        }
        if (texto.contains("-")) {
            return ConstantesValidaciones.ERROR_CEDULA_CON_GUION;
        } else {
            return validaCedula(texto);
        }

    }

    /**
     * Función que evalua que un texto dado sea un ruc
     *
     * @param texto el texto a evaluar
     * @return el resutlado de la validación (null = ok)
     */
    public static String validaRuc(String texto) {
        if (texto == null || texto.isEmpty()) {
            return ConstantesValidaciones.ERROR_RUC_VACIO_NULO;
        }
        //Valido el número de caracteres = 10
        if (texto.length() != ConstantesValidaciones.TAMANIO_RUC) {
            return ConstantesValidaciones.ERROR_RUC_TAMANIO;
        }

        String cedula = texto.substring(0, 10);
        String establecimiento = texto.substring(10, 13);

        //Valido que sea solo números
        if (validaSoloNumeros(texto, ConstantesValidaciones.TAMANIO_RUC) != null) {
            return ConstantesValidaciones.ERROR_RUC_NO_NUMEROS;
        }
        String validacionCedula = validaCedula(cedula);

        int numeroEstablecimiento = Integer.parseInt(establecimiento);

        if (validacionCedula != null || numeroEstablecimiento == 0) {
            return ConstantesValidaciones.ERROR_RUC_NO_VALIDO;
        } else {
            return null;
        }
    }

    /**
     * Función que valida que un cadena dada sea ruc o cédula válida
     *
     * @param texto el texto a evaluar
     * @return EL resultado de la validación (null = ok)
     */
    public static String validaCedulaRuc(String texto) {
        if (texto == null || texto.isEmpty()) {
            return ConstantesValidaciones.ERROR_CEDULA_RUC_VACIO_NULO;
        }
        if (texto.length() == 10 || texto.length() == 13) {
            if (texto.length() == 10) {
                if (validaCedula(texto) == null) {
                    return null;
                } else {
                    return ConstantesValidaciones.ERROR_CEDULA_RUC_NO_VALIDO;

                }
            } else {
                if (validaRuc(texto) == null) {
                    return null;
                } else {
                    return ConstantesValidaciones.ERROR_CEDULA_RUC_NO_VALIDO;
                }
            }
        } else {
            return ConstantesValidaciones.ERROR_CEDULA_RUC_TAMANIO;
        }
    }

    /**
     * Funcion que valida que un texto dado tenga solo números.
     *
     * @param texto   el texto a evaluar
     * @param tamanio El tamanio del texto, en caso de no validar el tamanio
     *                deberá ser null
     * @return null si el texto contiene solo numeros, caso contrario codigo de error
     */
    public static String validaSoloNumeros(String texto, Integer tamanio) {
        if (texto == null || texto.isEmpty()) {
            return ConstantesValidaciones.ERROR_SOLO_NUMEROS_VACIO_NULO;
        }
        //numero de digitos ilimitado
        String tamanioReg = ConstantesValidaciones.ZERO_OR_MORE;
        if (tamanio != null) {
            tamanioReg = "{" + tamanio + "}";
        }
        String regex = "\\d" + tamanioReg;
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(texto);
        return matcher.matches() ? null : ConstantesValidaciones.ERROR_SOLO_NUMEROS;
    }

    /**
     * Funcion que valida si un texto corresponde a una direccion de email.
     *
     * @param texto a evaluar
     * @return null en caso de email valido o el codigfo de error
     */
    public static String validaEmail(String texto) {
        if (texto == null || texto.isEmpty()) {
            return ConstantesValidaciones.ERROR_EMAIL_VACIO_NULO;
        }
        String regex = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(texto);
        return matcher.matches() ? null : ConstantesValidaciones.ERROR_EMAIL;
    }


    public static String validaCodigoDactilar(String texto) {

        if (texto.length() != ConstantesValidaciones.TAMANIO_COD_DACTILAR) {
            return ConstantesValidaciones.ERROR_CODIGO_DACTILAR;
        }
        String exp = "([AIXV0E][0-4][0-4][0-4][0-4][AIXV0E][0-4][0-4][0-4][0-4])";
        Pattern pattern = Pattern.compile(exp);
        Matcher matcher = pattern.matcher(texto);
        return matcher.matches() ? null : ConstantesValidaciones.ERROR_CODIGO_DACTILAR;
    }

    /**
     * Funcion que valida si un texto corresponde a un posible numero de celular
     *
     * @param celular
     * @return
     */
    public static String validaCelular(String celular) {
        if (celular == null || celular.isEmpty()) {
            return ConstantesValidaciones.ERROR_CELULAR_NULL_EMPTY;
        }
        //Valido el número de caracteres = 10
        if (celular.length() != ConstantesValidaciones.TAMANIO_CELULAR) {
            return ConstantesValidaciones.ERROR_TAMANIO_CELULAR;
        }

        //Valido que sea solo números
        if (validaSoloNumeros(celular, ConstantesValidaciones.TAMANIO_CELULAR) != null) {
            return ConstantesValidaciones.ERROR_CELULAR_NO_NUMEROS;
        }
        return null;
    }
}
