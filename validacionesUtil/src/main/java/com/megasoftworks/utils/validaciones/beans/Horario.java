/**
 * Clase Horario.java mar 22, 2019
 */
package com.megasoftworks.utils.validaciones.beans;

import java.util.Date;

/**
 * @author pabloaleman
 */
public class Horario {

    private final Date inicio;
    private final Date fin;

    public Horario(Date ini, Date fi) {
        this.inicio = ini;
        this.fin = fi;
    }

    public boolean intermedio(Date evaluar) {
        return evaluar.getTime() >= inicio.getTime() && evaluar.getTime() <= fin.getTime();
    }
}
