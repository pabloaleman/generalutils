/**
 * Clase ConstantesValidaciones.java mar 22, 2019
 */
package com.megasoftworks.utils.validaciones;

/**
 * @author pabloaleman
 */
public final class ConstantesValidaciones {

    public static final int TAMANIO_CEDULA = 10;
    public static final int TAMANIO_RUC = 13;
    public static final int TAMANIO_CELULAR = 10;
    public static final int TAMANIO_COD_DACTILAR = 10;

    public static final String ZERO_OR_MORE = "*";

    public static final String ERROR_CEDULA_TAMANIO = "001";
    public static final String ERROR_CEDULA_NO_NUMEROS = "002";
    public static final String ERROR_CEDULA_NO_VALIDA = "003";
    public static final String ERROR_CEDULA_CON_GUION = "004";
    public static final String ERROR_CEDULA_NULL_EMPTY = "005";


    public static final String ERROR_RUC_TAMANIO = "001";
    public static final String ERROR_RUC_NO_NUMEROS = "002";
    public static final String ERROR_RUC_NO_VALIDO = "003";
    public static final String ERROR_RUC_VACIO_NULO = "004";

    public static final String ERROR_CEDULA_RUC_TAMANIO = "001";
    public static final String ERROR_CEDULA_RUC_NO_VALIDO = "002";
    public static final String ERROR_CEDULA_RUC_VACIO_NULO = "003";

    public static final String ERROR_SOLO_NUMEROS = "001";
    public static final String ERROR_SOLO_NUMEROS_VACIO_NULO = "002";

    public static final String ERROR_EMAIL = "001";
    public static final String ERROR_EMAIL_VACIO_NULO = "002";

    public static final String ERROR_FORMATO_HORARIO = "001";
    public static final String ERROR_DIFERENCIA_HORARIO = "002";
    public static final String ERROR_CRUCE_HORARIOS = "003";

    public static final String ERROR_CODIGO_DACTILAR = "001";

    public static final String ERROR_FORMATO_INTERVALO = "001";
    public static final String ERROR_FECHA_INICIO_INTERVALO = "002";
    public static final String ERROR_FECHA_FIN_INTERVALO = "003";
    public static final String ERROR_FECHA_INICIO_MAYOR_A_FECHA_FIN_INTERVALO = "004";


    public static final String ERROR_TAMANIO_CELULAR = "001";
    public static final String ERROR_CELULAR_NO_NUMEROS = "002";
    public static final String ERROR_CELULAR_NULL_EMPTY = "003";

    private ConstantesValidaciones() {

    }
}
