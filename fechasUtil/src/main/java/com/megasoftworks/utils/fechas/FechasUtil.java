/**
 * Clase FechasUtil.java mar 19, 2019
 */
package com.megasoftworks.utils.fechas;

import com.megasoftworks.utils.fechas.exceptions.ConversionFechaException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

/**
 * @author pabloaleman
 */
public final class FechasUtil {

    public static final String FORMATO_FECHA = "dd/MM/yyyy";

    private static final Logger LOGGER = LoggerFactory.getLogger(FechasUtil.class);
    private static final int SEGUNDOS = 60;
    private static final int MINUTOS = 60;
    private static final int HORAS = 24;
    private static final int MILISEGUNDOS = 1000;

    public static final String MENSAJE_ERROR = "Error convirtiendo fecha";


    private FechasUtil() {
    }

    /**
     * Convierte una fecha a string en un formato dado.
     *
     * @param fecha   la fecha a convertir.
     * @param formato el formato de retorno.
     * @return El string de la fecha dada en el formato indicado.
     */
    public static String dateToString(Date fecha, String formato) {
        SimpleDateFormat formatoSDF = new SimpleDateFormat(formato);
        return formatoSDF.format(fecha);
    }

    /**
     * Convierte una fecha a string en un formato dado con un locale.
     *
     * @param fecha   la fecha a convertir.
     * @param formato el formato de retorno.
     * @param locale  el locale al cual se requiere la conversion.
     * @return El string de la fecha dada en el formato y locale indicado.
     */
    public static String dateToStringConLocale(Date fecha, String formato, Locale locale) {
        SimpleDateFormat formatoSDF = new SimpleDateFormat(formato, locale);
        return formatoSDF.format(fecha);
    }

    /**
     * Convierte una fecha a string en el formato dado con el lccale en Espanol.
     *
     * @param fecha   la fecha a convertir.
     * @param formato el formato de retorno.
     * @return El string de la fecha dada en el formato indicado en espanol.
     */
    public static String dateToStringEspanol(Date fecha, String formato) {
        SimpleDateFormat formatoSDF = new SimpleDateFormat(formato, new Locale("es", "ES"));
        return formatoSDF.format(fecha);
    }

    /**
     * Convierte una fecha a string en el formato dd/MM/yyyy.
     *
     * @param fecha la fecha a convertir.
     * @return El string de la fecha en formato dd/MM/yyyy.
     */
    public static String dateToString(Date fecha) {
        SimpleDateFormat formato = new SimpleDateFormat(FORMATO_FECHA);
        return formato.format(fecha);
    }

    /**
     * Convierte una fecha representada en un String a Date.
     *
     * @param fecha   la fecha a convertir.
     * @param formato el formato de la fecha.
     * @return la fecha en tipo Date.
     * @throws ConversionFechaException en caso de errores en la conversion.
     */
    public static Date stringToDate(String fecha, String formato) throws ConversionFechaException {
        try {
            SimpleDateFormat formatter = new SimpleDateFormat(formato);
            return formatter.parse(fecha);
        } catch (ParseException e) {
            LOGGER.error(MENSAJE_ERROR, e);
            throw new ConversionFechaException("Error convirtiendo string a fecha", e);
        }
    }


    /**
     * Convierte un String que representa una fecha en formato dd/MM/yyyy a date.
     *
     * @param fecha la fecha a convertir.
     * @return La fecha en tipo Date.
     * @throws ConversionFechaException en caso de errores en la conversion.
     */
    public static Date stringToDate(String fecha) throws ConversionFechaException {
        return stringToDate(fecha, FORMATO_FECHA);
    }

    /**
     * Setea la fecha dada en las horas, minutos y segundos a final del día de la fecha (23:59:59).
     *
     * @param fecha la fecha a setear el final del dia.
     * @return la fecha con horas, minutos y segundos en(23:59:59).
     */
    public static Date setFinalDia(Date fecha) {
        try {
            return stringToDate(String.format("%s 23:59:59", dateToString(fecha)), FORMATO_FECHA + " HH:mm:ss");
        } catch (ConversionFechaException e) {
            //nunca entrara a esta exception
            LOGGER.error(MENSAJE_ERROR, e);
            return fecha;
        }
    }

    /**
     * Setea la fecha dada en las horas, minutos y segundos a inicio del día de la fecha (00:00:00).
     *
     * @param fecha la fecha a setear el inicio del dia.
     * @return la fecha con horas, minutos y segundos en(00:00:00).
     */
    public static Date setInicioDia(Date fecha) {
        try {
            return stringToDate(String.format("%s 00:00:00", dateToString(fecha)), FORMATO_FECHA + " HH:mm:ss");
        } catch (ConversionFechaException e) {
            //nunca entrara a esta exception
            LOGGER.error(MENSAJE_ERROR, e);
            return fecha;
        }
    }

    /**
     * aumentoMinutos.
     * Funcion que recibe un Date y un numero de minutos y retorna un Date
     * incrementado el numero de minutos.
     *
     * @param actual  La fecha actual
     * @param minutos el numero de minutos a aumentar
     * @return El date aumentado los minutos
     */
    public static Date aumentoMinutos(Date actual, int minutos) {
        Date aux = new Date();
        aux.setTime(actual.getTime() + (SEGUNDOS * MILISEGUNDOS * minutos));
        return aux;
    }

    /**
     * Funcion que recibe una fecha y aumenta el numero de horas.
     *
     * @param fecha         la fecha a aumentar las horas.
     * @param horasAumentar el numero de horas a aumentar.
     * @return la fecha con el numero de horas aumentado.
     */
    public static Date aumentoHoras(Date fecha, int horasAumentar) {
        int horas = horasAumentar * MINUTOS * SEGUNDOS * MILISEGUNDOS;
        long finL = fecha.getTime() + Long.valueOf(horas);
        return new Date(finL);
    }

    /**
     * Funcion que recibe una fecha y aumenta el numero de dias.
     *
     * @param fecha la fecha a aumentar las horas
     * @param dias  el numero de dias a umentar en la fecha
     * @return
     */
    public static Date aumentaDias(Date fecha, int dias) {
        return aumentoHoras(fecha, HORAS * dias);
    }


    /**
     * Funcion que retorna un XMLGregorianCalendar a partir de una fecha.
     *
     * @param fecha la fecha a convertir.
     * @return la fecha convertida en tipo de dato XMLGregorianCalendar.
     */
    public static XMLGregorianCalendar dateToXMLGregorianCalendar(Date fecha) throws ConversionFechaException {
        try {
            GregorianCalendar gcFecha = new GregorianCalendar();
            gcFecha.setTime(fecha);
            return DatatypeFactory.newInstance().newXMLGregorianCalendar(gcFecha);
        } catch (DatatypeConfigurationException e) {
            LOGGER.error(MENSAJE_ERROR, e);
            throw new ConversionFechaException("Error convirtiendo string a fecha", e);
        }

    }

    /**
     * Funcion que convierte una fecha a TimesTamp.
     *
     * @param fecha la fecha a convertir.
     * @return la fecha en formato TimesTamp.
     */
    public static Timestamp dateToTimestamp(Date fecha) {
        return new Timestamp(fecha.getTime());
    }

}
