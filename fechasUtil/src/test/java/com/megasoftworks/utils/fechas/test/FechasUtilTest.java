/**
 * Clase FechasUtilTest.java mar 19, 2019
 */
package com.megasoftworks.utils.fechas.test;

import com.megasoftworks.utils.fechas.FechasUtil;
import com.megasoftworks.utils.fechas.exceptions.ConversionFechaException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.datatype.XMLGregorianCalendar;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Locale;

/**
 * @author pabloaleman
 */
public class FechasUtilTest {
    private static final Logger logger = LoggerFactory.getLogger(FechasUtilTest.class);
    private static final String fechaString = "21/03/2019 15:15:40";
    private static final String soloFechaString = "21/03/2019";
    private static final String fechaInicioString = "21/03/2019 00:00:00";
    private static final String fechaFinString = "21/03/2019 23:59:59";
    private static final String formatoFecha = "dd/MM/yyyy HH:mm:ss";
    private static final String formatoHora = "HH";
    private static final String formatoMinutos = "mm";
    private static final String formatoDiaAumentado = "dd";
    private static final String error = "Error en la prueba";
    private static final String errorCovnersion = "Error al convertir la fecha";
    private static final String formatoDia = "EEEEE";
    private static final String diaEsperado = "jueves";
    private static final int aumento = 1;
    private static final int minuto = 15;
    private static final int esperadoCero = 0;

    private static final String minutoSiguiente = "16";
    private static final String horaSiguiente = "16";
    private static final String diaSiguiente = "22";
    private static final Locale localeEspaniol = new Locale("es", "ES");

    private Date fecha;

    @Before
    public void inicia() {
        try {
            fecha = FechasUtil.stringToDate(fechaString, formatoFecha);

        } catch (ConversionFechaException e) {
            logger.error(errorCovnersion, e);
            Assert.fail(error);
        }
    }

    @Test
    public void dateToString() {
        String fechaRetornada = FechasUtil.dateToString(fecha, formatoFecha);
        Assert.assertEquals(error, fechaString, fechaRetornada);
    }

    @Test
    public void dateToStringConLocale() {
        String fechaRetornada = FechasUtil.dateToStringConLocale(fecha, formatoDia, localeEspaniol);
        Assert.assertEquals(error, diaEsperado, fechaRetornada);
    }

    @Test
    public void dateToStringEspanol() {
        String fechaRetornada = FechasUtil.dateToStringEspanol(fecha, formatoDia);
        Assert.assertEquals(error, diaEsperado, fechaRetornada);
    }

    @Test
    public void dateToStringFormat() {
        String fechaRetornada = FechasUtil.dateToString(fecha);
        Assert.assertEquals(error, soloFechaString, fechaRetornada);
    }

    @Test
    public void stringToDate() {
        try {
            Date fechaRetornada = FechasUtil.stringToDate(fechaString, formatoFecha);
            Assert.assertEquals(error, fecha, fechaRetornada);
        } catch (ConversionFechaException e) {
            logger.error(errorCovnersion, e);
            Assert.fail(error);
        }
    }

    @Test
    public void stringToDateConFormatoDefault() {
        try {
            Date fechaRetornada = FechasUtil.stringToDate(soloFechaString);
            String fechaRetornadaString = FechasUtil.dateToString(fechaRetornada);
            Assert.assertEquals(error, soloFechaString, fechaRetornadaString);
        } catch (ConversionFechaException e) {
            logger.error(errorCovnersion, e);
            Assert.fail(error);
        }
    }

    @Test
    public void setFinalDia() {
        Date fechaRetornada = FechasUtil.setFinalDia(fecha);
        String fechaRetornadaString = FechasUtil.dateToString(fechaRetornada, formatoFecha);
        Assert.assertEquals(error, fechaFinString, fechaRetornadaString);
    }

    @Test
    public void setInicioDia() {
        Date fechaRetornada = FechasUtil.setInicioDia(fecha);
        String fechaRetornadaString = FechasUtil.dateToString(fechaRetornada, formatoFecha);
        Assert.assertEquals(error, fechaInicioString, fechaRetornadaString);
    }

    @Test
    public void aumentoMinutos() {
        Date fechaAumentada = FechasUtil.aumentoMinutos(fecha, aumento);
        String minutosDeFechaRetornada = FechasUtil.dateToString(fechaAumentada, formatoMinutos);
        Assert.assertEquals(error, minutoSiguiente, minutosDeFechaRetornada);
    }

    @Test
    public void aumentoHoras() {
        Date fechaAumentada = FechasUtil.aumentoHoras(fecha, aumento);
        String horaDeFechaRetornada = FechasUtil.dateToString(fechaAumentada, formatoHora);
        Assert.assertEquals(error, horaSiguiente, horaDeFechaRetornada);
    }

    @Test
    public void aumentoDias() {
        Date fechaAumentada = FechasUtil.aumentaDias(fecha, aumento);
        String horaDeFechaRetornada = FechasUtil.dateToString(fechaAumentada, formatoDiaAumentado);
        Assert.assertEquals(error, diaSiguiente, horaDeFechaRetornada);
    }

    @Test
    public void dateToXMLGregorianCalendar() {
        try {
            XMLGregorianCalendar retornado = FechasUtil.dateToXMLGregorianCalendar(fecha);
            Assert.assertEquals(error, minuto, retornado.getMinute());
        } catch (ConversionFechaException e) {
            logger.error(errorCovnersion, e);
            Assert.fail(error);
        }
    }

    @Test
    public void dateToTimestamp() {
        Timestamp retornada = FechasUtil.dateToTimestamp(fecha);
        Assert.assertEquals(error, esperadoCero, retornada.compareTo(fecha));
    }
}
