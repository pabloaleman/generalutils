/**
 * Clase ClaveIncorrectaException.java mar 21, 2019
 */
package com.megasoftworks.utils.password.exceptions;

/**
 * @author pabloaleman
 */
public class ClaveIncorrectaException extends Exception {
    private static final long serialVersionUID = 4052279448675227958L;
}
