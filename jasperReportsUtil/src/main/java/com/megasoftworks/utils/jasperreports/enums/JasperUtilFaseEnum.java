package com.megasoftworks.utils.jasperreports.enums;

public enum JasperUtilFaseEnum {
	LECTURA,
	COMPILACION,
	GENERATE_PRINT,
	GENERATE_PDF_BYTE_ARRAY,
	GENERAL;
}
