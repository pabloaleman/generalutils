package com.megasoftworks.utils.jasperreports.exceptions;


import com.megasoftworks.utils.jasperreports.enums.JasperUtilFaseEnum;

public class JasperUtilException extends Exception {
	private static final long serialVersionUID = -2725360913670818970L;
	
	private final JasperUtilFaseEnum fase;

	public JasperUtilException(String message, JasperUtilFaseEnum fase) {
		super(message);
		this.fase = fase;
	}
	public JasperUtilFaseEnum getFase() {
		return fase;
	}
}
