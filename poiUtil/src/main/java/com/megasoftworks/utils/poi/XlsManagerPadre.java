/**
 * Clase XlsManagerPadre.java abr 01, 2019
 */
package com.megasoftworks.utils.poi;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;

import java.util.Iterator;

/**
 * @author pabloaleman
 */
public class XlsManagerPadre {

    /**
     * Funcion que recibe un iterador de filas y genera la cadena.
     *
     * @param rowIterator el iterador de lineas.
     * @param separator   el separador de columnas.
     * @return el String con el contenido.
     */
    protected static String barrerIterador(Iterator<Row> rowIterator, String separator) {
        StringBuilder sb = new StringBuilder();
        while (rowIterator.hasNext()) {
            Row row = rowIterator.next();
            // For each row, iterate through each columns
            Iterator<Cell> cellIterator = row.cellIterator();
            while (cellIterator.hasNext()) {
                Cell cell = cellIterator.next();
                switch (cell.getCellType()) {
                    case Cell.CELL_TYPE_STRING:
                        sb.append(cell.getStringCellValue());
                        sb.append(separator);
                        break;
                    case Cell.CELL_TYPE_NUMERIC:
                        String value = String.valueOf(cell.getNumericCellValue());
                        value = value.replace(",", ".");
                        sb.append(value);
                        sb.append(separator);
                        break;
                    case Cell.CELL_TYPE_BOOLEAN:
                        boolean valueB = cell.getBooleanCellValue();
                        String valueS = valueB ? "true" : "false";
                        sb.append(valueS);
                        sb.append(separator);
                        break;
                    default:
                }
            }
            sb.append("\n");
        }
        return sb.toString();
    }
}
