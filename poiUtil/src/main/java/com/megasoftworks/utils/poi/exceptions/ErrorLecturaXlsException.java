/**
 * Clase ErrorLecturaXlsException.java abr 01, 2019
 */
package com.megasoftworks.utils.poi.exceptions;

/**
 * @author pabloaleman
 */
public class ErrorLecturaXlsException extends Exception {
    public ErrorLecturaXlsException(Throwable t) {
        super(t);
    }
}
