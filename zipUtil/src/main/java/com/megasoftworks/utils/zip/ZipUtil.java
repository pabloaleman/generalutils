/**
 * Clase ZipUtil.java mar 25, 2019
 */
package com.megasoftworks.utils.zip;

import java.io.ByteArrayOutputStream;
import java.util.Collections;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.megasoftworks.utils.zip.beans.ArchivoComprimir;
import com.megasoftworks.utils.zip.exceptions.CreacionZipException;

/**
 * @author pabloaleman
 */
public final class ZipUtil {

    static final Logger LOGGER = LoggerFactory.getLogger(ZipUtil.class);

    private ZipUtil() {
    }

    /**
     * Funcion que reliza un archivo zip a partir de un archivo.
     * @param archivoComprimir el archivo a comprimir.
     * @return El bytearray del zip.
     * @throws CreacionZipException en caso de errores.
     */
    public static ByteArrayOutputStream zipArchivo(ArchivoComprimir archivoComprimir) throws CreacionZipException {
        return zipArchivos(Collections.singletonList(archivoComprimir));
    }

    /**
     * Funcion que reliza un archivo zip a partir de un listado de archivos.
     * @param archivosComprimir los archivos a comprimir.
     * @return El bytearray del zip.
     * @throws CreacionZipException en caso de errores.
     */
    public static ByteArrayOutputStream zipArchivos(List<ArchivoComprimir> archivosComprimir) throws CreacionZipException {
        try {
            ByteArrayOutputStream retorno = new ByteArrayOutputStream();
            ZipOutputStream zipOut = new ZipOutputStream(retorno);
            for (ArchivoComprimir archivoComprimir : archivosComprimir) {
                ZipEntry zipEntry = new ZipEntry(archivoComprimir.getNombre());
                zipOut.putNextEntry(zipEntry);

                byte[] bytes = new byte[1024];
                int length;
                while ((length = archivoComprimir.getContenido().read(bytes)) >= 0) {
                    zipOut.write(bytes, 0, length);
                }
                archivoComprimir.getContenido().close();
            }
            zipOut.close();
            retorno.close();
            return retorno;
        } catch (Exception e) {
            LOGGER.error("Error generando el archivo zip", e);
            throw new CreacionZipException(e);
        }
    }
}
