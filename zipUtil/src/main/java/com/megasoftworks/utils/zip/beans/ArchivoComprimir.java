/**
 * Clase ArchivoComprimir.java mar 26, 2019
 */
package com.megasoftworks.utils.zip.beans;

import java.io.InputStream;
import java.io.Serializable;

/**
 * @author pabloaleman
 */
public class ArchivoComprimir implements Serializable {

    private static final long serialVersionUID = -2060094220826506116L;
    private final InputStream contenido;
    private final String nombre;

    public ArchivoComprimir(InputStream is, String nombre) {
        this.contenido = is;
        this.nombre = nombre;
    }

    public InputStream getContenido() {
        return contenido;
    }
    public String getNombre() {
        return nombre;
    }
}
