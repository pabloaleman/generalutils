/**
 * Clase ZipUtilTest.java mar 25, 2019
 */
package com.megasoftworks.utils.zip.test;

import com.megasoftworks.utils.files.FilesUtil;
import com.megasoftworks.utils.files.exceptions.CrearArchivoException;
import com.megasoftworks.utils.zip.ZipUtil;
import com.megasoftworks.utils.zip.beans.ArchivoComprimir;
import com.megasoftworks.utils.zip.exceptions.CreacionZipException;
import org.junit.Assert;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * @author pabloaleman
 */
public class ZipUtilTest {
    @Test
    public void crearZip() {
        try {
            OutputStream retorno = ZipUtil.zipArchivo(new ArchivoComprimir(
                    new FileInputStream("src/test/resources/autorizacionMarcacionManual.pdf"),
                    "autorizacionMarcacionManual.pdf"));
            FilesUtil.crearArchivoDesdeBA(((ByteArrayOutputStream) retorno).toByteArray(),
                    "src/test/resources/salida.zip");
        } catch (FileNotFoundException | CreacionZipException | CrearArchivoException e) {
            Assert.fail("Error en la prueba");
        }
    }

    @Test
    public void crearZipVarios() {
        try {
            List<ArchivoComprimir> archivos = new ArrayList<>();
            archivos.add(new ArchivoComprimir(
                    new FileInputStream("src/test/resources/autorizacionMarcacionManual.pdf"),
                    "autorizacionMarcacionManual.pdf"));

            archivos.add(new ArchivoComprimir(
                    new FileInputStream("src/test/resources/DiagramaRDC.odt"), "DiagramaRDC.odt"));

            archivos.add(new ArchivoComprimir(
                    new FileInputStream("src/test/resources/rdc.png"), "rdc.png"));

            OutputStream retorno = ZipUtil.zipArchivos(archivos);
            FilesUtil.crearArchivoDesdeBA(((ByteArrayOutputStream) retorno).toByteArray(),
                    "src/test/resources/salidaVarios.zip");
        } catch (FileNotFoundException | CreacionZipException | CrearArchivoException e) {
            Assert.fail("Error en la prueba");
        }
    }
}
