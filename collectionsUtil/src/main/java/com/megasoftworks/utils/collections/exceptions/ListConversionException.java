/**
 * Clase ListConversionException.java mar 22, 2019
 */
package com.megasoftworks.utils.collections.exceptions;

/**
 * @author pabloaleman
 */
public class ListConversionException extends Exception {

    private static final long serialVersionUID = -8969694146653655664L;

    public ListConversionException(String mensaje, Throwable e) {
        super(mensaje, e);
    }
}
