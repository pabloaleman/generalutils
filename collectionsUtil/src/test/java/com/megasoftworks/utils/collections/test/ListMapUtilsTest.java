/**
 * Clase ListMapUtilsTest.java mar 22, 2019
 */
package com.megasoftworks.utils.collections.test;

import java.util.Arrays;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import com.megasoftworks.utils.collections.ListMapUtils;
import com.megasoftworks.utils.collections.exceptions.ListConversionException;

/**
 * @author pabloaleman
 */
public class ListMapUtilsTest {
    private static final String error = "Error en la prueba";
    private static final String cadena = "1, 2, 3, 4, 5";
    private static final String separador = ",";
    private static final List<Integer> enterosEsperados = Arrays.asList(1, 2, 3, 4, 5);
    private static final String[] enterosStringArray = {"1", "2", "3", "4", "5"};

    private static final List<Double> doubleEsperados = Arrays.asList(1.1, 2.2, 3.3, 4.4, 5.5);
    private static final String[] doubleStringArray = {"1.1", "2.2", "3.3", "4.4", "5.5"};

    @Test
    public void stringToIntegerList() {
        try {
            List<Integer> retorno = ListMapUtils.stringToIntegerList(cadena, separador);

            Assert.assertEquals(error, enterosEsperados, retorno);
        } catch (ListConversionException e) {
            Assert.fail(error);
        }
    }

    @Test
    public void stringToIntegerListCadenaNull() {
        try {
            ListMapUtils.stringToIntegerList(null, separador);
            Assert.fail(error);
        } catch (ListConversionException e) {
            Assert.assertNotNull(e);

        }
    }

    @Test
    public void stringToIntegerListSeparadorNull() {
        try {
            ListMapUtils.stringToIntegerList(cadena, null);
            Assert.fail(error);
        } catch (ListConversionException e) {
            Assert.assertNotNull(e);
        }
    }

    @Test
    public void stringToIntegerListCadenaNoValida() {
        try {
            ListMapUtils.stringToIntegerList(error, separador);
            Assert.fail(error);
        } catch (ListConversionException e) {
            Assert.assertNotNull(e);
        }
    }

    @Test
    public void stringArrayToIntegerList() {
        try {
            List<Integer> retorno = ListMapUtils.stringArrayToIntegerList(enterosStringArray);
            Assert.assertEquals(error, enterosEsperados, retorno);
        } catch (ListConversionException e) {
            Assert.fail(error);
        }
    }

    @Test
    public void stringArrayToDoubleList() {
        try {
            List<Double> retorno = ListMapUtils.stringArrayToDoubleList(doubleStringArray);
            Assert.assertEquals(error, doubleEsperados, retorno);
        } catch (ListConversionException e) {
            Assert.fail(error);
        }
    }
}
